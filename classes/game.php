<?php
class Game {

	private $mysqlconnect;
	
	public function __construct()
    {   
        $this->mysqlconnect=new mysqli('localhost', 'root', '','welcometo');
        if ($this->mysqlconnect->connect_errno)
            die("Connection to database failed");
    }
	
    function host($inputCode) {
        $err = [];
        $code = $this->mysqlconnect->real_escape_string($inputCode);
        
        if(!preg_match("/^[a-zA-Z0-9]+$/",$code)) $err[] = "Код может состоять только из букв английского алфавита и цифр";
		if(strlen($code) < 3 || strlen($code) > 20) $err[] = "Код должен быть не меньше 3-х символов и не больше 20";
        $query = $this->mysqlconnect->query("SELECT ID FROM sessions WHERE CODE='".$code."'");
		if(mysqli_num_rows($query) > 0) $err[] = "Не удалось создать игру с таким кодом";
        
        if(count($err) == 0)
		{
            $plans = [rand(1,6),rand(1,6),rand(1,6)];
            $deck = range(1, 81);
            shuffle($deck);
            
            $deck = implode($deck,',');
            
			$this->mysqlconnect->query("INSERT INTO sessions SET CODE='".$code."',CARDS='".$deck."',PLAN1='".$plans[0]."',PLAN2='".$plans[1]."',PLAN3='".$plans[2]."'");
            //printf("Сообщение ошибки: %s\n", $this->mysqlconnect->error);
            
            
            $result['RESULT'] = true;
        }
		else
		{
            $result['RESULT'] = false;
            $result['MESSAGE'] = $err;
        }
        return $result;
        
    }
    
    public function connect($inputUsername, $inputCode)
	{
		$err = [];
		$username = $this->mysqlconnect->real_escape_string($inputUsername);
		$code = $this->mysqlconnect->real_escape_string($inputCode);

		// проверям логин
		if(!preg_match("/^[a-zA-Z0-9]+$/",$username))
		{
			$err[] = "Логин может состоять только из букв английского алфавита и цифр";
		}

		if(strlen($username) < 3 or strlen($username) > 15)
		{
			$err[] = "Логин должен быть не меньше 3-х символов и не больше 15";
		}
		
		// проверяем, не сущестует ли игры с таким кодом
		$query = $this->mysqlconnect->query("SELECT ID,TURN FROM sessions WHERE CODE='".$code."'");
		if(mysqli_num_rows($query) == 0)
		{
			$err[] = "Игры с таким кодом не существует";
		}
		if(mysqli_fetch_all($query)[0][1] != 1)
		{
			$err[] = "Игра уже идёт";
		}
		
		// Если нет ошибок, то добавляем в БД нового пользователя
		if(count($err) == 0)
		{
			$this->mysqlconnect->query("INSERT INTO players SET NAME='".$username."', SESSION_ID=(SELECT ID from sessions WHERE CODE='".$code."')");
			$sessionData = $this->mysqlconnect->query("SELECT * from sessions WHERE CODE='".$code."'");
            $sessionData = mysqli_fetch_all($sessionData)[0];
			$playerID = $this->mysqlconnect->query("SELECT `AUTO_INCREMENT` FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'welcometo' AND TABLE_NAME = 'players'");
            
            $_SESSION['ID'] = mysqli_fetch_all($playerID)[0][0]-1;
            $_SESSION['username'] = $username;
            $_SESSION['session'] = $sessionData[0];
            $_SESSION['plans'] = [$sessionData[4],$sessionData[5],$sessionData[6]];
            $_SESSION['deck'] = explode(',',$sessionData[2]);
            $_SESSION['turn'] = 0;
            
			$result['RESULT'] = true;
		} else {
            $result['RESULT'] = false;
            $result['MESSAGE'] = $err;
        }
        return $result;
	}
    
    static function parseCity($str) {
        return 33-substr_count($str, "-");
    }
    
    /***********
    *
    * INPUT:  json{'city','lastaction','value'}
    * OUTPUT: json{array('playername','plan1','plan2','plan3','houses','refuses','wps')}
    *
    ***********/
    
    function endTurn($jsonData) {
        $data = json_decode($jsonData,true);
        
        $data["houses"] = 33-substr_count($data['city'], "-");
        
        //debug param
        $data["turn"] = $_SESSION['turn'];
        
        //Отмечаем, что игрок завершил ход
        $this->mysqlconnect->query("UPDATE players 
                                    SET TURN = TURN + 1, 
                                        CITY='".$data["city"]."', 
                                        HOUSE_AMOUNT='".$data["houses"]."', 
                                        PLAN1='".$data["p1"]."', 
                                        PLAN2='".$data["p2"]."', 
                                        PLAN3='".$data["p3"]."', 
                                        WP_AMOUNT='".$data["wp"]."',
                                        REFUSAL_AMOUNT='".$data["refuses"]."'
                                    WHERE ID='".$_SESSION['ID']."'");
        //print_r($this->mysqlconnect->error);
        if($data["refuses"] == 3) $this->mysqlconnect->query("UPDATE sessions SET TURN = 995 WHERE ID='".$_SESSION['session']."'");
        if($_SESSION['turn'] == 29) $this->mysqlconnect->query("UPDATE sessions SET TURN = 996 WHERE ID='".$_SESSION['session']."'");
        if($data["p1"]!='' && $data["p2"]!='' && $data["p3"]!='') $this->mysqlconnect->query("UPDATE sessions SET TURN = 997 WHERE ID='".$_SESSION['session']."'");
        
    }
    
    function idle() {
        //Проверяем, все ли завершили ход 
        //Есть TURN игрока и общий TURN сессии
        //Если TURN'ы равны, то игрок готов к следующему ходу
        //Если все игроки готовы, TURN сессии увеличивается на 1
        //Если игрок, отметив завершение хода, видит, что его TURN на 1 меньше сессии, он делает начинает ход
        
        $sessionTurn = $this->mysqlconnect->query("SELECT TURN from sessions WHERE ID='".$_SESSION['session']."'");
        $sessionTurn = mysqli_fetch_all($sessionTurn)[0][0];
        $playersData = $this->mysqlconnect->query("SELECT * from players WHERE SESSION_ID='".$_SESSION['session']."'");
        $playersData = mysqli_fetch_all($playersData);
        
        if($sessionTurn < 990) {
            
            $diffTotalAndReady = 0;
            if (count($playersData) != 1) {
                foreach($playersData as $player) {
                        $diffTotalAndReady++;
                        if ($player[10] == $sessionTurn) $diffTotalAndReady--;
                }
            }
            if(($_SESSION['turn']+2) == $sessionTurn) $diffTotalAndReady = 0;
            
            //Начинаем новый ход, если все готовы
            if($diffTotalAndReady == 0) {
                if(($_SESSION['turn']+2) != $sessionTurn) $this->mysqlconnect->query("UPDATE sessions SET TURN = TURN+1 WHERE ID='".$_SESSION['session']."'");
                ++$_SESSION['turn'];
                $data["cards"] = [[$_SESSION['deck'][$_SESSION['turn']*3],$_SESSION['deck'][$_SESSION['turn']*3+1],$_SESSION['deck'][$_SESSION['turn']*3+2]],
                                  [$_SESSION['deck'][($_SESSION['turn']-1)*3],$_SESSION['deck'][($_SESSION['turn']-1)*3+1],$_SESSION['deck'][($_SESSION['turn']-1)*3+2]]];
                $data["state"] = "ready";
                $data["players"] = $playersData;
                print_r(json_encode($data));
            } else print_r('{"state":"idling"}');   
        } else {
            if ($sessionTurn == 995) $data["end"] = "Один из игроков не может больше строить дома.";
            if ($sessionTurn == 996) $data["end"] = "Закончилась колода.";
            if ($sessionTurn == 997) $data["end"] = "Один из игроков выполнил все планы застройки.";
            
            $data["cards"] = [[$_SESSION['deck'][$_SESSION['turn']*3],$_SESSION['deck'][$_SESSION['turn']*3+1],$_SESSION['deck'][$_SESSION['turn']*3+2]],
                                  [$_SESSION['deck'][($_SESSION['turn']-1)*3],$_SESSION['deck'][($_SESSION['turn']-1)*3+1],$_SESSION['deck'][($_SESSION['turn']-1)*3+2]]];
            $data["state"] = "ready";
            $data["players"] = $playersData;
            print_r(json_encode($data));
        }
    }
    
    function getEndGameStats($id) {
        $playersData = $this->mysqlconnect->query("SELECT * from players WHERE SESSION_ID='".$id."'");
        $playersData = mysqli_fetch_all($playersData);
        return $playersData;
    }
    
    function setScore($value) {
        $this->mysqlconnect->query("UPDATE players SET TURN = ".$value." WHERE ID='".$_SESSION['ID']."'");
    }
    
    function getPlayers() {
        $sql = $this->mysqlconnect->query("SELECT * from players WHERE SESSION_ID='".$_SESSION['session']."'");
        $data["players"] = mysqli_fetch_all($sql);
        $data["turn"] = $_SESSION['turn'];
        print_r(json_encode($data));
    }
}?>