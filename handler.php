<?
    session_start();
    require('classes/game.php');
    $game=new Game();
    if(isset($_GET["ready"])) {
        $game->endTurn(file_get_contents("php://input"));
    } else if(isset($_GET["idle"])) {
        $game->idle();
    } else if(isset($_GET["score"])) {
        $game->setScore();
    } else if(isset($_GET["waiting"])) {
        $game->getPlayers();
    }
?>