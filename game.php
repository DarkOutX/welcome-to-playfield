<?session_start();?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<title>Welcome To...</title>

	<meta name="description" content="Welcome To... digital boardgame">
	<meta name="tags" content="game, boardgame, welcometo, digital">

	<link rel="stylesheet" type="text/css" href="assets/css/styles.css">
	<link rel="stylesheet" type="text/css" href="assets/css/tstyles.css">
	<link rel="stylesheet" type="text/css" href="assets/css/plist.css">
	<link rel="stylesheet" type="text/css" href="assets/css/mlist.css">
	<link rel="stylesheet" type="text/css" href="assets/css/blank.css">
	<link rel="stylesheet" type="text/css" href="assets/css/modalWindow.css">
	<link rel="shortcut icon" type="image/png" href="favicon.ico"/>
	
    <script>
        const ID = <?=$_SESSION['ID']?>;
        const SESSION_PLANS = [<?=$_SESSION['plans'][0]?>,<?=$_SESSION['plans'][1]?>,<?=$_SESSION['plans'][2]?>];
        <?if($_SESSION['turn']==0):?>
            var TURN_CARDS = [[<?=$_SESSION['deck'][0]?>, <?=$_SESSION['deck'][1]?>, <?=$_SESSION['deck'][2]?>],
                              [<?=$_SESSION['deck'][78]?>,<?=$_SESSION['deck'][79]?>,<?=$_SESSION['deck'][80]?>]];
        <?else:?>
            var TURN_CARDS = [[<?=$_SESSION['deck'][$_SESSION['turn']*3]?>,<?=$_SESSION['deck'][$_SESSION['turn']*3+1]?>,<?=$_SESSION['deck'][$_SESSION['turn']*3+2]?>],
                              [<?=$_SESSION['deck'][($_SESSION['turn']-1)*3]?>,<?=$_SESSION['deck'][($_SESSION['turn']-1)*3+1]?>,<?=$_SESSION['deck'][($_SESSION['turn']-1)*3+2]?>]];
        <?endif;?>
    </script>
    <script src="assets/js/jquery-3.3.1.min.js"></script>
	<script src="assets/js/effects.js"></script>
	<script src="assets/js/game.js"></script>
	<script src="assets/js/modal.js"></script>
    
   
	<?require('assets/lang/ru.php');?>
</head>
<body onLoad="beginGame();">
    <div id="build-variants" style="display: none;">
        <div id="var0" class="build-variant-single"></div>
        <div id="var1" class="build-variant-single"></div>
        <div id="var2" class="build-variant-single"></div>
        <div id="var3" class="build-variant-single"></div>
        <div id="var4" class="build-variant-single"></div>
    </div>
    <div id="agency-adder" style="display: none;">
        <div id="ag1" class="agency-adder-single">
            <img src='assets/img/icons/ar1.png' alt='' title=''/>
            <div class='agency-adder-cur' max=1>0</div>
        </div>
        <div id="ag2" class="agency-adder-single">
            <img src='assets/img/icons/ar2.png' alt='' title=''/>
            <div class='agency-adder-cur' max=2>0</div>
        </div>
        <div id="ag3" class="agency-adder-single">
            <img src='assets/img/icons/ar3.png' alt='' title=''/>
            <div class='agency-adder-cur' max=3>0</div>
        </div>
        <div id="ag4" class="agency-adder-single">
            <img src='assets/img/icons/ar4.png' alt='' title=''/>
            <div class='agency-adder-cur' max=4>0</div>
        </div>
        <div id="ag5" class="agency-adder-single">
            <img src='assets/img/icons/ar5.png' alt='' title=''/>
            <div class='agency-adder-cur' max=4>0</div>
        </div>
        <div id="ag6" class="agency-adder-single">
            <img src='assets/img/icons/ar6.png' alt='' title=''/>
            <div class='agency-adder-cur' max=4>0</div>
        </div>
    </div>
    <div id="left-top-block">
        <div id="menu" class="menu-start bg-wooden" onClick="toggleSpecial('menu');">
            <ul id="menu-list">
                <li><a href="index.php?logout">Выйти</a></li>
            </ul>
            <div id="menu-name" class="menu-name-start">МЕНЮ</div>
        </div>
        <div id="players" class="players-start bg-wooden" onClick="toggleSpecial('players');">
            <div id="players-others">
                <!--
                <div id="player-1" class="player-info">
                    <span class="player-name">DarkOutX</span>
                    
                    <div class="icon-plan-done"></div>
                    <div class="icon-plan-done"></div>
                    <div class="icon-plan"></div>
                    
                    <span class="player-houses">13/33</span>
                    <div class="icon-house"></div>
                    
                    <div class="icon-fail-done"></div>
                    <div class="icon-fail"></div>
                    <div class="icon-fail"></div>
                    
                    <span class="player-wp">5</span>
                    <div class="icon-wp"></div>
                </div>
                -->
            </div>
            <div id="player-main" class="player-info">
                <span class="player-name"><?=$_SESSION['username']?></span>
                
                <div class="icon-plan"></div>
                <div class="icon-plan"></div>
                <div class="icon-plan"></div>
                
                <span class="player-houses">0/33</span>
                <div class="icon-house"></div>
                
                <div class="icon-fail"></div>
                <div class="icon-fail"></div>
                <div class="icon-fail"></div>
                
                <span class="player-wp">0</span>
                <div class="icon-wp"></div>
                
            </div>
        </div>
    </div>
    <div id="cards">
        <div class="card_table">
            <div class="card_single deck_house">
                <span id="house1_it" class="house_index_top" ></span>
                <div id="house1_et" class="house_effect_top"></div>
                <span id="house1_im" class="house_index_main"></span>
                <div id="house1_eb" class="house_effect_bot"></div>
                <span id="house1_ib" class="house_index_bot" ></span>
            </div>
            <div class="card_single deck_house">
                <span id="house2_it" class="house_index_top" ></span>
                <div id="house2_et" class="house_effect_top"></div>
                <span id="house2_im" class="house_index_main"></span>
                <div id="house2_eb" class="house_effect_bot"></div>
                <span id="house2_ib" class="house_index_bot" ></span>
            </div>
            <div class="card_single deck_house">
                <span id="house3_it" class="house_index_top" ></span>
                <div id="house3_et" class="house_effect_top"></div>
                <span id="house3_im" class="house_index_main"></span>
                <div id="house3_eb" class="house_effect_bot"></div>
                <span id="house3_ib" class="house_index_bot" ></span>
            </div>
            </br>
            <div class="card_single deck_effect">
                <div id="effect1" class="effect"></div>
            </div>
            <div class="card_single deck_effect">
                <div id="effect2" class="effect"></div>
            </div>
            <div class="card_single deck_effect">
                <div id="effect3" class="effect"></div>
            </div>
        </div>
        <div id='SuperPower' charges="0">
            SWAP
        </div>
    </div>
	<section id="block-right">
		<div id="plans" class="plans-start bg-wooden" onClick="toggleSpecial('plans');">
			<span id='plans-title'><?=$text['plans'];?></span>
			<div class="plan_table">
				<div id="plan1" class="card_single deck_plan_uncompleted" type="1">
					<div id="plan1_ds" class="plan_description" ></div>
					<div id="plan1_pf" class="plan_points_first"></div>
					<div id="plan1_pl" class="plan_points_left" ></div>
				</div>
				<div id="plan2" class="card_single deck_plan_uncompleted" type="2">
					<div id="plan2_ds" class="plan_description" ></div>
					<div id="plan2_pf" class="plan_points_first"></div>
					<div id="plan2_pl" class="plan_points_left" ></div>
				</div>
				<div id="plan3" class="card_single deck_plan_uncompleted" type="3">
					<div id="plan3_ds" class="plan_description" ></div>
					<div id="plan3_pf" class="plan_points_first"></div>
					<div id="plan3_pl" class="plan_points_left" ></div>
				</div>
			</div>
		</div>
		<div id="blank" class="blank-start" onClick="">
            <div id="street-block">
                <div class="street" id="street-one">
                    <div class="building fence" id="h11" minNum='0' maxNum='17'><span></span></div>
                    <div class="building"       id="h12" minNum='0' maxNum='17'><span></span></div>
                    <div class="building pool"  id="h13" minNum='0' maxNum='17'><span></span></div>
                    <div class="building"       id="h14" minNum='0' maxNum='17'><span></span></div>
                    <div class="building"       id="h15" minNum='0' maxNum='17'><span></span></div>
                    <div class="building"       id="h16" minNum='0' maxNum='17'><span></span></div>
                    <div class="building pool"  id="h17" minNum='0' maxNum='17'><span></span></div>
                    <div class="building pool"  id="h18" minNum='0' maxNum='17'><span></span></div>
                    <div class="building"       id="h19" minNum='0' maxNum='17'><span></span></div>
                    <div class="building"       id="h1a" minNum='0' maxNum='17'><span></span></div>
                </div>
                <div class="street" id="street-two">
                    <div class="building pool fence" minNum='0' maxNum='17'><span></span></div>
                    <div class="building" minNum='0' maxNum='17'><span></span></div>
                    <div class="building" minNum='0' maxNum='17'><span></span></div>
                    <div class="building pool" minNum='0' maxNum='17'><span></span></div>
                    <div class="building" minNum='0' maxNum='17'><span></span></div>
                    <div class="building" minNum='0' maxNum='17'><span></span></div>
                    <div class="building" minNum='0' maxNum='17'><span></span></div>
                    <div class="building pool" minNum='0' maxNum='17'><span></span></div>
                    <div class="building" minNum='0' maxNum='17'><span></span></div>
                    <div class="building" minNum='0' maxNum='17'><span></span></div>
                    <div class="building" minNum='0' maxNum='17'><span></span></div>
                </div>
                <div class="street" id="street-three">
                    <div class="building fence" minNum='0' maxNum='17'><span></span></div>
                    <div class="building pool" minNum='0' maxNum='17'><span></span></div>
                    <div class="building" minNum='0' maxNum='17'><span></span></div>
                    <div class="building" minNum='0' maxNum='17'><span></span></div>
                    <div class="building" minNum='0' maxNum='17'><span></span></div>
                    <div class="building" minNum='0' maxNum='17'><span></span></div>
                    <div class="building pool" minNum='0' maxNum='17'><span></span></div>
                    <div class="building" minNum='0' maxNum='17'><span></span></div>
                    <div class="building" minNum='0' maxNum='17'><span></span></div>
                    <div class="building" minNum='0' maxNum='17'><span></span></div>
                    <div class="building pool" minNum='0' maxNum='17'><span></span></div>
                    <div class="building" minNum='0' maxNum='17'><span></span></div>
                </div>
            </div>
            <div id="park-block">
                <div class="park-line" id="park-line-one">
                    <div class="park-dot" crossed=0></div>
                    <div class="park-dot" crossed=0></div>
                    <div class="park-dot" crossed=0></div>
                </div>
                <div class="park-line" id="park-line-two">
                    <div class="park-dot" crossed=0></div>
                    <div class="park-dot" crossed=0></div>
                    <div class="park-dot" crossed=0></div>
                    <div class="park-dot" crossed=0></div>
                </div>
                <div class="park-line" id="park-line-three">
                    <div class="park-dot" crossed=0></div>
                    <div class="park-dot" crossed=0></div>
                    <div class="park-dot" crossed=0></div>
                    <div class="park-dot" crossed=0></div>
                    <div class="park-dot" crossed=0></div>
                </div>
            </div>
            <div id="scores-block">
                <div id="scores-plans">
                    <div class="scores-plan">0</div>
                    <div class="scores-plan">0</div>
                    <div class="scores-plan">0</div>
                </div>
                <div id="scores-parks">
                    <div class="scores-park">0</div>
                    <div class="scores-park">0</div>
                    <div class="scores-park">0</div>
                </div>
                <div id="scores-pools">
                    <div class="scores-pool"></div>
                    <div class="scores-pool"></div>
                    <div class="scores-pool"></div>
                    <div class="scores-pool"></div>
                    <div class="scores-pool"></div>
                    <div class="scores-pool"></div>
                    <div class="scores-pool"></div>
                    <div class="scores-pool"></div>
                    <div class="scores-pool"></div>
                </div>
                <div id="scores-wps">
                    <div class="scores-wp"></div>
                    <div class="scores-wp"></div>
                    <div class="scores-wp"></div>
                    <div class="scores-wp"></div>
                    <div class="scores-wp"></div>
                    <div class="scores-wp"></div>
                    <div class="scores-wp"></div>
                    <div class="scores-wp"></div>
                    <div id="scores-wps-middle">
                        <div class="scores-wp"></div>
                        <div class="scores-wp"></div>
                        <div class="scores-wp"></div>
                    </div>
                </div>
                <div id="scores-agency">
                    <div class="scores-agency-cols">
                        <div class="scores-agency-col">
                            <div class="scores-agency-single"></div>
                        </div>
                        <div class="scores-agency-col">
                            <div class="scores-agency-single"></div>
                            <div class="scores-agency-single"></div>
                        </div>
                        <div class="scores-agency-col">
                            <div class="scores-agency-single"></div>
                            <div class="scores-agency-single"></div>
                            <div class="scores-agency-single"></div>
                        </div>
                        <div class="scores-agency-col">
                            <div class="scores-agency-single"></div>
                            <div class="scores-agency-single"></div>
                            <div class="scores-agency-single"></div>
                            <div class="scores-agency-single"></div>
                        </div>
                        <div class="scores-agency-col">
                            <div class="scores-agency-single"></div>
                            <div class="scores-agency-single"></div>
                            <div class="scores-agency-single"></div>
                            <div class="scores-agency-single"></div>
                        </div>
                        <div class="scores-agency-col">
                            <div class="scores-agency-single"></div>
                            <div class="scores-agency-single"></div>
                            <div class="scores-agency-single"></div>
                            <div class="scores-agency-single"></div>
                        </div>
                    </div>
                    <div id="scores-agency-row">
                        <div class="scores-agency-single-amount">0</div>
                        <div class="scores-agency-single-amount">0</div>
                        <div class="scores-agency-single-amount">0</div>
                        <div class="scores-agency-single-amount">0</div>
                        <div class="scores-agency-single-amount">0</div>
                        <div class="scores-agency-single-amount">0</div>
                    </div>
                </div>
                <div id="scores-BIS">
                    <div class="scores-BIS-single"></div>
                    <div class="scores-BIS-single"></div>
                    <div class="scores-BIS-single"></div>
                    <div class="scores-BIS-single"></div>
                    <div class="scores-BIS-single"></div>
                    <div class="scores-BIS-single"></div>
                    <div class="scores-BIS-single"></div>
                    <div class="scores-BIS-single"></div>
                    <div class="scores-BIS-single"></div>
                </div>
                <div id="scores-fails">
                    <div class="scores-fail"></div>
                    <div class="scores-fail"></div>
                    <div class="scores-fail"></div>
                </div>
            </div>
            <div id="total-scores-block">
                <div class="total-scores-group" id="total-score-plans">0</div>
                <div class="total-scores-group" id="total-score-parks">0</div>
                <div class="total-scores-group" id="total-score-pools">0</div>
                <div class="total-scores-group" id="total-score-wps">0</div>
                <div class="total-scores-group" id="total-score-agency">
                    <div class="total-score-agency-one-area" id="total-score-agency-1">0</div>
                    <div class="total-score-agency-one-area" id="total-score-agency-2">0</div>
                    <div class="total-score-agency-one-area" id="total-score-agency-3">0</div>
                    <div class="total-score-agency-one-area" id="total-score-agency-4">0</div>
                    <div class="total-score-agency-one-area" id="total-score-agency-5">0</div>
                    <div class="total-score-agency-one-area" id="total-score-agency-6">0</div>
                </div>
                <div class="total-scores-group" id="total-score-BIS">0</div>
                <div class="total-scores-group" id="total-score-fails">0</div>
                <div class="total-scores-group" id="total-score-all">0</div>
            </div> 
		</div>
	</section>
</body>
</html>