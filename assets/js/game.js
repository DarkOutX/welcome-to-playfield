﻿var cards = '{"1":{"house":"1","effect":"2"},"2":{"house":"11","effect":"2"},"3":{"house":"4","effect":"3"},"4":{"house":"1","effect":"0"},"5":{"house":"3","effect":"5"},"6":{"house":"10","effect":"2"},"7":{"house":"9","effect":"4"},"8":{"house":"2","effect":"1"},"9":{"house":"5","effect":"0"},"10":{"house":"2","effect":"2"},"11":{"house":"10","effect":"4"},"12":{"house":"9","effect":"3"},"13":{"house":"4","effect":"1"},"14":{"house":"7","effect":"4"},"15":{"house":"4","effect":"4"},"16":{"house":"12","effect":"2"},"17":{"house":"8","effect":"5"},"18":{"house":"15","effect":"0"},"19":{"house":"8","effect":"2"},"20":{"house":"11","effect":"0"},"21":{"house":"8","effect":"4"},"22":{"house":"10","effect":"1"},"23":{"house":"5","effect":"2"},"24":{"house":"12","effect":"5"},"25":{"house":"3","effect":"0"},"26":{"house":"7","effect":"1"},"27":{"house":"7","effect":"3"},"28":{"house":"8","effect":"3"},"29":{"house":"12","effect":"4"},"30":{"house":"8","effect":"1"},"31":{"house":"9","effect":"5"},"32":{"house":"9","effect":"0"},"33":{"house":"6","effect":"5"},"34":{"house":"8","effect":"0"},"35":{"house":"6","effect":"4"},"36":{"house":"4","effect":"5"},"37":{"house":"14","effect":"1"},"38":{"house":"7","effect":"0"},"39":{"house":"5","effect":"2"},"40":{"house":"7","effect":"2"},"41":{"house":"6","effect":"0"},"42":{"house":"7","effect":"2"},"43":{"house":"5","effect":"0"},"44":{"house":"12","effect":"1"},"45":{"house":"9","effect":"2"},"46":{"house":"10","effect":"0"},"47":{"house":"4","effect":"2"},"48":{"house":"6","effect":"0"},"49":{"house":"13","effect":"4"},"50":{"house":"13","effect":"3"},"51":{"house":"13","effect":"0"},"52":{"house":"8","effect":"1"},"53":{"house":"13","effect":"5"},"54":{"house":"7","effect":"5"},"55":{"house":"6","effect":"2"},"56":{"house":"5","effect":"1"},"57":{"house":"9","effect":"1"},"58":{"house":"9","effect":"2"},"59":{"house":"15","effect":"2"},"60":{"house":"6","effect":"3"},"61":{"house":"10","effect":"5"},"62":{"house":"14","effect":"2"},"63":{"house":"5","effect":"1"},"64":{"house":"2","effect":"0"},"65":{"house":"11","effect":"1"},"66":{"house":"6","effect":"1"},"67":{"house":"1","effect":"1"},"68":{"house":"7","effect":"1"},"69":{"house":"14","effect":"0"},"70":{"house":"11","effect":"1"},"71":{"house":"11","effect":"0"},"72":{"house":"10","effect":"3"},"73":{"house":"8","effect":"2"},"74":{"house":"12","effect":"3"},"75":{"house":"9","effect":"1"},"76":{"house":"10","effect":"0"},"77":{"house":"3","effect":"4"},"78":{"house":"3","effect":"3"},"79":{"house":"8","effect":"0"},"80":{"house":"11","effect":"2"},"81":{"house":"15","effect":"1"}}';
var plansOLD = 
'{"1":{"1":{"description":"Complete house estates (2*size 6)","firstPoints":"10","leftPoints":"6","isHard":"0"},"2":{"description":"Complete house estates (3*size 3)","firstPoints":"8","leftPoints":"4","isHard":"0"},"3":{"description":"Complete house estates (4*size 2)","firstPoints":"8","leftPoints":"4","isHard":"0"},"4":{"description":"Complete house estates (2*size 4)","firstPoints":"6","leftPoints":"3","isHard":"0"},"5":{"description":"Complete house estates (6*size 1)","firstPoints":"8","leftPoints":"4","isHard":"0"},"6":{"description":"Complete house estates (2*size 5)","firstPoints":"8","leftPoints":"4","isHard":"0"},"7":{"description":"Build all houses on the first street","firstPoints":"6","leftPoints":"3","isHard":"1"},"8":{"description":"Build all houses on the third street","firstPoints":"8","leftPoints":"4","isHard":"1"},"9":{"description":"Build the first and last house of each street","firstPoints":"7","leftPoints":"4","isHard":"1"},"10":{"description":"Hire 7 temps.","firstPoints":"6","leftPoints":"3","isHard":"1"},"11":{"description":"Build 5 BIS on the same street.","firstPoints":"8","leftPoints":"3","isHard":"1"}},"2":{"1":{"description":"Complete house estates (size 3, size 6)","firstPoints":"8","leftPoints":"4","isHard":"0"},"2":{"description":"Complete house estates (size 4, size 5)","firstPoints":"9","leftPoints":"5","isHard":"0"},"3":{"description":"Complete house estates (2*size 2, size 5)","firstPoints":"10","leftPoints":"6","isHard":"0"},"4":{"description":"Complete house estates (2*size 3, size 4)","firstPoints":"12","leftPoints":"7","isHard":"0"},"5":{"description":"Complete house estates (3*size 1, size 4)","firstPoints":"9","leftPoints":"5","isHard":"0"},"6":{"description":"Complete house estates (3*size 1, size 6)","firstPoints":"11","leftPoints":"6","isHard":"0"},"7":{"description":"Build all the pools and all the parks and one roundabout on the samestreet","firstPoints":"10","leftPoints":"5","isHard":"1"},"8":{"description":"Build all the pools and all the parks on the secondstreet","firstPoints":"8","leftPoints":"3","isHard":"1"},"9":{"description":"Build all the pools and all the parks on the thirdstreet","firstPoints":"10","leftPoints":"5","isHard":"1"},"10":{"description":"Build all the pools on 2 streets","firstPoints":"7","leftPoints":"4","isHard":"1"},"11":{"description":"Build all the parks on 2 streets","firstPoints":"7","leftPoints":"4","isHard":"1"}},"3":{"1":{"description":"Complete house estates (size 3, size 4)","firstPoints":"7","leftPoints":"3","isHard":"0"},"2":{"description":"Complete house estates (size 2, size 5)","firstPoints":"7","leftPoints":"3","isHard":"0"},"3":{"description":"Complete house estates (size 1, size 4, size 5)","firstPoints":"13","leftPoints":"7","isHard":"0"},"4":{"description":"Complete house estates (size 2, size 3, size 5)","firstPoints":"13","leftPoints":"7","isHard":"0"},"5":{"description":"Complete house estates (size 1, size 2, size 6)","firstPoints":"12","leftPoints":"7","isHard":"0"},"6":{"description":"Complete house estates (size 1, 2*size 2, size 3)","firstPoints":"11","leftPoints":"6","isHard":"0"}}}';
var plans = 
'{"1": { "1": { "description": "Complete house estates (2*size 6)", "firstPoints": "10", "leftPoints": "6", "objective": {"6":"2"} }, "2": { "description": "Complete house estates (3*size 3)", "firstPoints": "8", "leftPoints": "4", "objective": {"3":"3"} }, "3": { "description": "Complete house estates (4*size 2)", "firstPoints": "8", "leftPoints": "4", "objective": {"2":"4"} }, "4": { "description": "Complete house estates (2*size 4)", "firstPoints": "6", "leftPoints": "3", "objective": {"4":"2"} }, "5": { "description": "Complete house estates (6*size 1)", "firstPoints": "8", "leftPoints": "4", "objective": {"1":"6"} }, "6": { "description": "Complete house estates (2*size 5)", "firstPoints": "8", "leftPoints": "4", "objective": {"5":"2"} }},"2": { "1": { "description": "Complete house estates (size 3, size 6)", "firstPoints": "8", "leftPoints": "4", "objective": {"3":"1","6":"1"} }, "2": { "description": "Complete house estates (size 4, size 5)", "firstPoints": "9", "leftPoints": "5", "objective": {"4":"1","5":"1"} }, "3": { "description": "Complete house estates (2*size 2, size 5)", "firstPoints": "10", "leftPoints": "6", "objective": {"2":"2","5":"1"} }, "4": { "description": "Complete house estates (2*size 3, size 4)", "firstPoints": "12", "leftPoints": "7", "objective": {"3":"2","4":"1"} }, "5": { "description": "Complete house estates (3*size 1, size 4)", "firstPoints": "9", "leftPoints": "5", "objective": {"1":"3","4":"1"} }, "6": { "description": "Complete house estates (3*size 1, size 6)", "firstPoints": "11", "leftPoints": "6", "objective": {"1":"3","6":"1"} }},"3": { "1": { "description": "Complete house estates (size 3, size 4)", "firstPoints": "7", "leftPoints": "3", "objective": {"3":"1","4":"1"} }, "2": { "description": "Complete house estates (size 2, size 5)", "firstPoints": "7", "leftPoints": "3", "objective": {"2":"1","5":"1"} }, "3": { "description": "Complete house estates (size 1, size 4, size 5)", "firstPoints": "13", "leftPoints": "7", "objective": {"1":"1","4":"1","5":"1"} }, "4": { "description": "Complete house estates (size 2, size 3, size 5)", "firstPoints": "13", "leftPoints": "7", "objective": {"2":"1","3":"1","5":"1"} }, "5": { "description": "Complete house estates (size 1, size 2, size 6)", "firstPoints": "12", "leftPoints": "7", "objective": {"1":"1","2":"1","6":"1"} }, "6": { "description": "Complete house estates (size 1, 2*size 2, size 3)", "firstPoints": "11", "leftPoints": "6", "objective": {"1":"1","2":"2","3":"1"} }}}';
plans = JSON.parse(plans);
//console.log(plans);
cards = JSON.parse(cards);
const debug = true;
const TOTAL_CARDS = 81;
const TOTAL_PLANS_1 = 11;
const TOTAL_PLANS_2 = 11;
const TOTAL_PLANS_3 = 6;
const HARD_PLANS_1 = 5;
const HARD_PLANS_2 = 5;
var curCard;
var isHard = false;
function completePlan(planType) {
    if(document.getElementById("plan"+ planType).className === "card_single deck_plan_uncompleted") {
        document.getElementById("plan"+ planType +"_pf").style.visibility = 'hidden';
        document.getElementById("plan"+ planType).className = "card_single deck_plan_completed";
    }
}
function setPlan(planType, planNum) {
    document.getElementById("plan"+ planType +"_ds").innerHTML = plans[planType][planNum]['description'];
    document.getElementById("plan"+ planType +"_pf").innerHTML = plans[planType][planNum]['firstPoints'];
    document.getElementById("plan"+ planType +"_pl").innerHTML = plans[planType][planNum]['leftPoints' ];
}
function setPlansOld() {
	var j;
	if (isHard) {
		j = Math.floor(Math.random() * (TOTAL_PLANS_1)) + 1;
        setPlan(1,j);
		j = Math.floor(Math.random() * (TOTAL_PLANS_2)) + 1;
        setPlan(2,j);
	} else {
		j = Math.floor(Math.random() * (TOTAL_PLANS_1 - HARD_PLANS_1)) + 1;
        setPlan(1,j);
		j = Math.floor(Math.random() * (TOTAL_PLANS_2 - HARD_PLANS_2)) + 1;
        setPlan(2,j);
	}
	j = Math.floor(Math.random() * (TOTAL_PLANS_3)) + 1;
    setPlan(3,j);
}
function setPlans() {
	setPlan(1,SESSION_PLANS[0]);
    setPlan(2,SESSION_PLANS[1]);
	setPlan(3,SESSION_PLANS[2]);
}
function setTripleHouses() {
	for (i = 1; i <= 3; i++) {
		document.getElementById("house"+ i +"_it").innerHTML = cards[TURN_CARDS[0][i-1]]['house'];
		//document.getElementById("house"+ i +"_et").className = "house_effect_top effect_"+ cards[card + i - 1]['effect'];
		//document.getElementById("house"+ i +"_et").className = "house_effect_top";
		document.getElementById("house"+ i +"_im").innerHTML = cards[TURN_CARDS[0][i-1]]['house'];
		//document.getElementById("house"+ i +"_eb").className = "house_effect_bot";
		document.getElementById("house"+ i +"_ib").innerHTML = cards[TURN_CARDS[0][i-1]]['house'];
		document.getElementById("house"+ i +"_et").parentNode.setAttribute("effect",cards[TURN_CARDS[0][i-1]]['effect']);
	}
}
function setTripleEffects() {
	document.getElementById("effect1").innerHTML = "<div class='effect' effect="+cards[TURN_CARDS[1][0]]['effect']  +"></div>";
	document.getElementById("effect2").innerHTML = "<div class='effect' effect="+cards[TURN_CARDS[1][1]]['effect']+"></div>";
	document.getElementById("effect3").innerHTML = "<div class='effect' effect="+cards[TURN_CARDS[1][2]]['effect']+"></div>";
}
function beginGame() {
    console.log("Begin Game");
    console.log(TURN_CARDS[0]);
    curCard = 1;
	setPlans();
	nextTurn();
}
function nextTurn(){
    setTripleEffects();
    setTripleHouses();
}