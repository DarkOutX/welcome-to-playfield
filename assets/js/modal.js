const points = {
        "parks"   :[[0,2,4,10],[0,2,4,6,14],[0,2,4,6,8,18]],
        "pools"   :[0,3,6,9,13,17,21,26,31,36],
        "agencies":[[1,3],[2,3,4],[3,4,5,6],[4,5,6,7,8],[5,6,7,8,10],[6,7,8,10,12]],
        "BIS"     :[0,1,3,6,9,12,16,20,24,28],
        "refuses" :[0,0,3,5]
    };
    
function setScores() {
    var totalScore = 0;
    //parks
    var totalParksScore = 0;
    $(".scores-park").each(function(i){
        let line = $(".park-line")[i];
        $(this).html(points["parks"][i][$(line).children(".park-dot[crossed=1]").length]);
        totalParksScore += points["parks"][i][$(line).children(".park-dot[crossed=1]").length];
    });
    $("#total-score-parks").html(totalParksScore);
    totalScore += totalParksScore;
    //pools
    totalScore += points["pools"][$(".scores-pool[crossed=1]").length];
    $("#total-score-pools").html(points["pools"][$(".scores-pool[crossed=1]").length]);
    //wps
    let arr = [];
    $("span.player-wp").each(function(i){
        arr.push(parseInt($(this).html()));
        arr.sort();
        arr.reverse();
    });
    var place = 1;
    for(let i = 0; i<arr.length; i++) {
        if($(".scores-wp[crossed=1]").length == arr[i]) {
            break;
        } else if(place>3){
            break;
        } else {
            place++;
        }
    }
    if(place == 1) {
        totalScore += 7;
        $("#total-score-wps").html(7);
    } else if (place == 2) {
        totalScore += 4;
        $("#total-score-wps").html(4);
    } else if (place == 3) {
        totalScore += 1;
        $("#total-score-wps").html(1);
    } else {
        $("#total-score-wps").html(0);
    }
    //agencies
    $(".scores-agency-col").each(function(i){
        let modif = points["agencies"][i][$(this).children(".scores-agency-single[crossed=1]").length];
        let curAmount = $(".scores-agency-single-amount")[i];
        curAmount = parseInt($(curAmount).html()) * modif;
        $("#total-score-agency-" +(i+1)).html(curAmount);
        totalScore+=curAmount;
    });
    //bis
    totalScore -= points["BIS"][$(".scores-BIS-single[crossed=1]").length];
    $("#total-score-BIS").html(points["BIS"][$(".scores-BIS-single[crossed=1]").length]);
    //refuses
    totalScore -= points["refuses"][$(".scores-fail[crossed=1]").length];
    $("#total-score-fails").html(points["refuses"][$(".scores-fail[crossed=1]").length]);   
    //total
    $("#total-score-all").html(totalScore);
}
    
$(document).ready(function() {    
    var version = '2.5';
    var curBuilding = null;
    var curAction = 'setHouse';
    function showVariants(min, max, WP = false) {
            
            $("#build-variants").toggle();
            $("#build-variants").css('left',event.pageX + 'px');
            $("#build-variants").css('top',event.pageY + 'px');

            $("#var1").hide();
            $("#var2").hide();
            $("#var3").hide();
            
            $("#var0").hide();
            $("#var4").hide();
            
            if (!WP) {
                let v1 = parseInt($("#house1_im").text());
                let v2 = parseInt($("#house2_im").text());
                let v3 = parseInt($("#house3_im").text());
                
                if ((v1 >= min) && (v1 <= max))  $("#var1").show();
                if ((v2 >= min) && (v2 <= max))  $("#var2").show();
                if ((v3 >= min) && (v3 <= max))  $("#var3").show();

                $("#var1").html($("#house1_im").text());
                $("#var1").attr("effect",$("#effect1").children("div").attr("effect"));

                $("#var2").html($("#house2_im").text());
                $("#var2").attr("effect",$("#effect2").children("div").attr("effect"));

                $("#var3").html($("#house3_im").text());
                $("#var3").attr("effect",$("#effect3").children("div").attr("effect"));
            } else {
                
                $(".build-variant-single").attr("effect",4);
                $("#var0").html(WP);
                $("#var1").html(WP+1);
                $("#var2").html(WP+2);
                $("#var3").html(WP-1);
                $("#var4").html(WP-2);
                
                if ( (WP >= min)  &&  (WP <= max) )  $("#var0").show();
                if ((WP+1 >= min) && (WP+1 <= max))  $("#var1").show();
                if ((WP+2 >= min) && (WP+2 <= max))  $("#var2").show();
                if ((WP-2 >= min) && (WP-2 <= max))  $("#var3").show();
                if ((WP-1 >= min) && (WP-1 <= max))  $("#var4").show();
                
                $(".build-variant-single").show();
                //curAction = 'setHouse';
            }

    }
    
    function idle() {
        $.ajax({
            method: "POST",
            url: "handler.php?idle",
            data: '',
        })
        .done(function(msg) {
            //console.log((msg));
            let data = jQuery.parseJSON(msg);
            console.log(data["state"]);
            if(data["state"] == "ready") {
                drawTable(data['players']);
                TURN_CARDS = data['cards'];
                nextTurn();
                checkRefuse();
                if(data["end"] != null) {
                    setScores();
                    sendScore();
                    alert("Игра завершена!\n"+data["end"]+"\nВаш счет: "+$("#total-score-all").html());
                    window.location.href = 'index.php?stat';
                }
            } else {
                setTimeout(idle, 1000);
            };
        });
    }
    function sendScore() {
        $.ajax({
            method: "POST",
            url: "handler.php?score="+$("#total-score-all").html(),
        })
        .done(function(msg) {
            
        });
    }
    function drawTable(pData) {
        var otherP = $("#players-others");
            $(otherP).html("");
            otherP = document.getElementById('players-others');
        var mainP = $("#player-main");
            $(mainP).html("");
        $(pData).each(function(i){
            if(pData[i][0] == ID) {
                var totalDiv = document.getElementById('player-main');
            } else {
                var totalDiv = document.createElement('div');
                totalDiv.setAttribute('id',"player-"+pData[i][0]);
                totalDiv.setAttribute('class',"player-info");
            }
            
            let tag = document.createElement('span');
            tag.setAttribute('class',"player-name");
            tag.innerHTML = pData[i][1];
            totalDiv.appendChild(tag);
            
            for(let g=6; g<=8; g++) {
                tag = document.createElement('div');
                if (pData[i][g] != 0) tag.setAttribute('class',"icon-plan-done");
                                else  tag.setAttribute('class',"icon-plan");
                totalDiv.appendChild(tag);             
            }
            
            tag = document.createElement('span');
            tag.setAttribute('class',"player-houses");
            tag.innerHTML = pData[i][4]+"/33";
            totalDiv.appendChild(tag);
            tag = document.createElement('div');
            tag.setAttribute('class',"icon-house");
            totalDiv.appendChild(tag);
            
            for(let g=1; g<=3; g++) {
                tag = document.createElement('div');
                if (pData[i][9] >= g) tag.setAttribute('class',"icon-fail-done");
                                else  tag.setAttribute('class',"icon-fail");
                totalDiv.appendChild(tag);             
            }
            
            tag = document.createElement('span');
            tag.setAttribute('class',"player-wp");
            tag.innerHTML = pData[i][5];
            totalDiv.appendChild(tag);
            tag = document.createElement('div');
            tag.setAttribute('class',"icon-wp");
            totalDiv.appendChild(tag);
                
            if(pData[i][0] != ID) otherP.appendChild(totalDiv);
        });
    }
    
    function checkRefuse() {
        let index1 = $(".house_index_main")[0];
        let index2 = $(".house_index_main")[1];
        let index3 = $(".house_index_main")[2];
        let nums = [parseInt($(index1).html()),parseInt($(index2).html()),parseInt($(index3).html())];
        let allowed = false;
        $(".building").each(function(i){
            let curB = $(".building")[i];
            if($(curB).children("span").html() == '') {
                if(parseInt($(curB).attr("maxNum"))>=nums[0] && parseInt($(curB).attr("minNum"))<=nums[0]) allowed = true;
                if(parseInt($(curB).attr("maxNum"))>=nums[1] && parseInt($(curB).attr("minNum"))<=nums[1]) allowed = true;
                if(parseInt($(curB).attr("maxNum"))>=nums[2] && parseInt($(curB).attr("minNum"))<=nums[2]) allowed = true; 
            }
        });
        if (!allowed) {
            crossLast($(".scores-fail"));
            turnEnd();
        }
    }
    
    function turnEnd() {
        
        setScores();
        let plan1 = $(".scores-plan")[0];
        let plan2 = $(".scores-plan")[1];
        let plan3 = $(".scores-plan")[2];
        let jsonStr = '{"city":"'+getStr()+'","wp":'+$(".scores-wp[crossed=1]").length+',"p1":'+$(plan1).html()+',"p2":'+$(plan2).html()+',"p3":'+$(plan3).html()+',"refuses":'+$(".scores-fail[crossed=1]").length+'}';
        
        $.ajax({
            method: "POST",
            url: "handler.php?ready",
            data: jsonStr,
            contentType: "application/json"
        })
        .done(function(msg) {
            //console.log((msg));
        });
        
        idle();
        
    }
    
    $(".building").on("click", function(ev){
        curBuilding = ev.target;
        if ((curAction == 'setHouse') && $(ev.target).children('span').text() == '') {
            showVariants($(curBuilding).attr('minNum'),$(curBuilding).attr('maxNum'));
        } else if (curAction == 'setFence') {
            checkFence();
        } else if (curAction == 'setBIS') {
            checkBis();
        }
    });
    
    function crossLast(arr) {
        $(arr).each(function(){
            if ($(this).attr('crossed') != 1) {
                $(this).attr('crossed','1'); 
                return false;
            }
        });
        return true;
    }
    
    //BLOCK OF EFFECTS
    function checkPark() {
        let parkLine = $("#"+$(curBuilding).parent().attr('id').replace('street-','park-line-'));
        crossLast($(parkLine).children());
        turnEnd();
    }
    function checkPool() {
        if ($(curBuilding).attr('class').indexOf('pool') > 0) {
            crossLast($('#scores-pools').children());
            $(curBuilding).toggleClass('pool pool-checked');
        }
        turnEnd();
    }
    function checkAgency() {
        curAction = 'setAgency';
        $("#agency-adder").show();
    }
    function checkFence() {
        if (!$(curBuilding).hasClass("fence")) {
            if (confirm("Вы точно хотите построить забор? (Слева от выбранного дома)")) {
                $(curBuilding).toggleClass('fence');
                getStr();
                curAction = 'setHouse';
                turnEnd();
            }
        }
    }
    function checkWp(num) {
        //curAction = 'setWP';
        //showVariants($(curBuilding).attr('minNum'),$(curBuilding).attr('maxNum'),num);
        
        if (($(".scores-wp[crossed=1]").length % 3) == 1) {
            crossLast($("#scores-wps-middle .scores-wp"));
        } else {
            crossLast($(".scores-wp"));
        }
        turnEnd();
    }
    function checkBis() {
        if(!$(curBuilding).prev().text() == "" && !$(curBuilding).hasClass("fence") && !$(curBuilding).prev().hasClass("BIS")) {
            setHouse(parseInt($(curBuilding).prev().text()), true);
            crossLast($(".scores-BIS-single"));
            curAction = 'setHouse';
            turnEnd();
            //console.log("BIS."+$(curBuilding).prev().text());
        } else if (!$(curBuilding).next().text() == "" && !$(curBuilding).next().hasClass("fence") && !$(curBuilding).next().hasClass("BIS")) {
            setHouse(parseInt($(curBuilding).next().text()), true);
            crossLast($(".scores-BIS-single"));
            curAction = 'setHouse';
            turnEnd();
            //console.log("BIS."+$(curBuilding).next().text());
        } else {
            //console.log("Нельзя построить пристройку");
        }
    }
    
    //END OF EFFECTS BLOCK
    
    $(".build-variant-single").on("click", function(ev){
        let curNum = parseInt($(ev.target).html());
        let curEffect = $(ev.target).attr('effect');
        
        //if (curEffect != 4) setHouse(curNum);
        setHouse(curNum)
        //BEGIN: Checking effects
            switch(curEffect) {
                case '0': //fence 
                    curAction = 'setFence';
                    break;
                case '1': //agency
                    checkAgency();
                    break;
                case '2': //park 
                    checkPark();
                    break;
                case '3': //pool
                    checkPool();
                    break;
                case '4': //wp
                    checkWp(curNum);
                    break;
                case '5': //bis
                    curAction = 'setBIS';
                    //checkBis();
                    break;
            }
        //END: Checking effects
        
        
        curBuilding = null;
        $("#build-variants").toggle();
    });
    
    function setHouse(num, isBis = false) {
        let lBuild = $(curBuilding).prev();
        let rBuild = $(curBuilding).next();
        $(curBuilding).children("span").html(num);
        $(curBuilding).attr('maxNum',num);
        $(curBuilding).attr('minNum',num);
                
        while($(lBuild).children('span').html() == '') {
            $(lBuild).attr('maxNum',num-1);
            lBuild = $(lBuild).prev();
        }
        while($(rBuild).children('span').html() == '') {
            $(rBuild).attr('minNum',num+1);
            rBuild = $(rBuild).next();
        }
        if(isBis) $(curBuilding).toggleClass("BIS");
    }
    
    $("#build-variants").on('mouseleave',function(ev){
		if ($("#build-variants").css('display') != 'none') $("#build-variants").toggle();
	});
    
    $dbg = function(act = '') {
        if (act != '') curAction = act;
            else curAction = 'setHouse';
        console.log(curAction);
    }
    
    $setNums = function() {
        $('.building').children("span").html(7);
        console.log(curAction);
    }
    
    $showStr = function() {
        console.log(getStr());
    }
    
    $(".scores-agency-col").each(function(curSize){
        let curLvl = 0;
        $(this).children().each(function(){
            if ($(this).attr('crossed')) ++curLvl;
        });
        let curDiv = $(".agency-adder-cur")[curSize];
        $(curDiv).html(curLvl);
    });
    
    $(".agency-adder-single").on('mouseenter',function(ev){
        if (curAction === 'setAgency' && !$(ev.target).children("div").attr('completed')) {
            let curNum = parseInt($(ev.target).children("div").html());
            $(ev.target).children("div").html(++curNum);
        }
	});
    $(".agency-adder-single").on('mouseleave',function(ev){
		if (curAction === 'setAgency' && !$(ev.target).children("div").attr('completed')) {
            let curNum = parseInt($(ev.target).children("div").html());
            $(ev.target).children("div").html(--curNum);
        }
	});
    $(".agency-adder-single").on('click',function(ev){
		if (curAction === 'setAgency' && !$(ev.target).children("div").attr('completed')) { 
            if ($(ev.target).children("div").html() == $(ev.target).children("div").attr('max')) $(ev.target).children("div").attr('completed','true');
            curAction = 'setHouse'; 
            let curAgencyLine = $(".scores-agency-col")[parseInt($(ev.target).attr('id').replace("ag","")) - 1];
            crossLast($(curAgencyLine).children());
            $("#agency-adder").hide();
            turnEnd();
        }
    });

    $("#SuperPower").on("click", function(ev){
        if(parseInt($("#SuperPower").attr('charges'))==1) {
            let oldEffects = [$("#effect1").children("div").attr("effect"),$("#effect2").children("div").attr("effect"),$("#effect3").children("div").attr("effect")];
            let newOrder = prompt("Введите новый порядок эффектов слева-направо, используя цифры 1,2 и 3.\nТекущий порядок: 123\nБудьте внимательны! Бонус можно применить только 1 раз!");
            newOrder = newOrder.split('');
            $("#effect1").children("div").attr("effect",oldEffects[parseInt(newOrder[0])-1]);
            $("#effect2").children("div").attr("effect",oldEffects[parseInt(newOrder[1])-1]);
            $("#effect3").children("div").attr("effect",oldEffects[parseInt(newOrder[2])-1]);
            $("#SuperPower").hide();
        } else {
            alert("Постройте 5 домов с подряд идущими номерами, чтобы открыть бонус. \nБонус позволит поменять местами карты эффектов");
        }
    });
    
	console.log('Modal v' + version);
    //debug func to open blank
    //abba();
});

var zones;
function checkForZones(streetArr) {
    
    //plans[1][SESSION_PLANS[0]].objective[0]
    //plans[2][SESSION_PLANS[1]].objective[0]
    //plans[3][SESSION_PLANS[2]].objective[0]
    
    
    zones = {1:0,2:0,3:0,4:0,5:0,6:0};
    let curLen = 0;
    $(streetArr).each(function(i){
        if((streetArr[i] != '|') && (streetArr[i] != '.')) {
            if (streetArr[i] != '-') {
                if((streetArr[i-1] == '|') || curLen) ++curLen;
                if(curLen>6) curLen = 0;                        
                if((streetArr[i+1] == '|') && curLen) {
                    //console.log('Найдена зона длиной '+curLen);
                    ++zones[curLen];
                    let blank = $(".scores-agency-single-amount")[curLen-1];
                    $(blank).html(zones[curLen]);
                    curLen = 0;
                }
            } else curLen = 0;
        }
    });
    //Проверка целей
    for(let i=1;i<=3;i++) {
        
        let isDone = true;
        let obj = plans[i][SESSION_PLANS[i-1]].objective;
        for(let j=0;j<Object.keys(obj).length;j++) {
            //console.log(Object.keys(obj)[j] + " - " + obj[Object.keys(obj)[j]]);
            if(zones[Object.keys(obj)[j]]<obj[Object.keys(obj)[j]]) isDone = false;
        }
        
        if(isDone) {
            for(let j=0;j<Object.keys(obj).length;j++) {
                zones[Object.keys(obj)[j]]=zones[Object.keys(obj)[j]]-obj[Object.keys(obj)[j]];
            }
            let curPlan = $(".scores-plan")[i-1];
            if(parseInt($(curPlan).html())==0) {
                console.log("Цель выполнена!");
                if($("#plan"+i).hasClass("deck_plan_uncompleted")) $(curPlan).html($("#plan"+i).children(".plan_points_first").html())
                    else $(curPlan).html($("#plan"+i).children(".plan_points_left").html());
                completePlan(i);
                alert("Вы выполнили цель "+i+"!");
            }
        }
    }
    
    console.log(zones);
    return zones;
}

function getOneStr(sel) {
    //console.log(sel);
    var numInRow = 1;
    var lastNum = 0;
    let totalStr = '';
    $(sel).children(".building").each( function(i) {
        let houseNum = $(sel).children(".building").children("span")[i].innerHTML;
        let houseFence = $(sel).children(".building")[i];
        houseFence = $(houseFence).attr('class').indexOf('fence') > 0;
        if(houseFence) totalStr += '|';
                    else   totalStr += '.';
        if(houseNum == '') {    
            totalStr += '-';
            numInRow = 0;
        } else {
            totalStr += parseInt(houseNum).toString(18);
            if(parseInt($("#SuperPower").attr('charges')) == 0) {
                if(parseInt(houseNum)-1 == lastNum) ++numInRow;
                if(numInRow == 5) {
                    alert("Вам доступен бонус SWAP");
                    $("#SuperPower").attr('charges','1');
                }
            }
        }
        lastNum = parseInt(houseNum);
    });
    numInRow = 1;
    totalStr+='|';
    //return totalStr.slice(0,-1)+"|";
    //checkStreetForZones(totalStr.split(""));
    return totalStr;
}

function getStr() {
    let totalStr = '';
    totalStr += getOneStr('#street-one');
    totalStr += getOneStr('#street-two');
    totalStr += getOneStr('#street-three');
    checkForZones(totalStr.split(""));
    return totalStr;
}