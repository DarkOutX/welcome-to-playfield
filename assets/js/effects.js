$('#blank').click(function() {
	$('#blank').animate({
		bottom: '285px'
	}, 200);
});

function abba() {
	if($('#blank').attr('class') == 'blank-shown') {
		$('#blank').attr("class","blank-hidden");
	} else {
		$('#blank').attr("class","blank-shown");
	}
}
function togglenew(objname) {
	var obj = $('#'+objname);
	if(objname == "blank") {
		if($(obj).attr('class') == objname+'-shown') {
			$(obj).attr('class',objname+'-hidden');
			$(obj).animate({bottom: '=320px'});
		} else {
			if($('#plans').attr('class') != 'plans-hidden') toggle('plans');
			$(obj).attr('class',objname+'-shown');
			$(obj).animate({bottom: '=0px'});
		}
	} else {
		if($(obj).attr('class') == objname+'-hidden') {
			if($('#blank').attr('class') == 'blank-shown') toggle('blank');
			$(obj).attr('class',objname+'-shown');
			$(obj).animate({top: '=0px'});
		} else {
			$(obj).attr('class',objname+'-hidden');
			$(obj).animate({top: '=-320px'});
		}
	}
}
function toggleSpecial(objname) {
	var obj = $('#'+objname);
	if(objname != "plans") {
		if(!($(obj).attr('class').includes(objname+'-shown'))) {
			//if(!($('#plans').attr('class').includes('plans-hidden')) && (objname == 'blank')) toggleSpecial('plans');
			if(objname == 'menu') {
                $('#menu-name').slideToggle(300);
            }
            $(obj).attr('class',objname+'-shown bg-wooden');
		} else {
			if(objname == 'menu') {
                $('#menu-name').slideToggle(300);
            }
            $(obj).attr('class',objname+'-hidden bg-wooden');
		}
	} else {
		if($(obj).attr('class').includes(objname+'-hidden')) {
			if($('#blank').attr('class').includes('blank-shown')) toggleSpecial('blank'); 
			$(obj).attr('class',objname+'-shown bg-wooden');
		} else {
            toggleSpecial('blank'); 
			$(obj).attr('class',objname+'-hidden bg-wooden');
		}
	}
}
function toggleOld(objname) {
	var obj = $('#'+objname);
	if(objname != "plans") {
		if($(obj).attr('class') != objname+'-shown') {
			if(($('#plans').attr('class') != 'plans-hidden') && (objname == 'blank')) toggle('plans');
			$(obj).attr('class',objname+'-shown');
		} else {
			$(obj).attr('class',objname+'-hidden');
		}
	} else {
		if($(obj).attr('class') == objname+'-hidden') {
			if($('#blank').attr('class') == 'blank-shown') toggle('blank'); 
			$(obj).attr('class',objname+'-shown');
		} else {
			$(obj).attr('class',objname+'-hidden');
		}
	}
}