<?php
class Game {

//                                                                           
// M\   /M    M    MM  MM    MM      MMMMM   MM      MMMMMMM  MMMMMM  MM  MM 
// MM\ /MM   MMM   __  MMMM  MM      MM  MM  MM      MM   MM  MM      MM MM  
// MM\V/MM  MM MM  MM  MM MM MM      MMMMM   MM      MM   MM  MM      MMMM   
// MM V MM  MmmmM  MM  MM  MMMM      MM  MM  MM      MM   MM  MM      MM MM  
// MM   MM  M   M  MM  MM    MM      MMMMM   MMMMMM  MMMMMMM  MMMMMM  MM  MM 
//                                                                           
	
	//[table]: sessions
	//ID
	//CODE
	//CARDS
	//CUR_CARDS
	//P1
	//P2
	//P3
	
	//[table]: 	players
	//ID
	//NAME
	//SESSION_ID
	//P1
	//P2
	//P3
	//FAILS
	//HOUSES
	//WORK_PLACES
	
	private $username;
	private $code;
	
	private $mysqlconnect;
	
	public function __construct()
    {   
        $this->mysqlconnect=new mysqli('localhost', 'root', '','welcometo');
        if ($this->mysqlconnect->connect_errno)
            die("Connection to database failed");
    }
	
//                                                                             
//   M    MM   MM  MMMMMM  MM  MM      MMMMM   MM      MMMMMMM  MMMMMM  MM  MM 
//  MMM   MM   MM  ""MM""  MM  MM      MM  MM  MM      MM   MM  MM      MM MM  
// MM MM  MM   MM    MM    MMMMMM      MMMMM   MM      MM   MM  MM      MMMM   
// MmmmM  MMM MMM    MM    MM  MM      MM  MM  MM      MM   MM  MM      MM MM  
// M   M   MMMMM     MM    MM  MM      MMMMM   MMMMMM  MMMMMMM  MMMMMM  MM  MM 
//                                                                              
          
	public function connect($username, $code)
	{
		$err = [];
		$this->username = $this->mysqlconnect->real_escape_string($username);
		$this->code = $this->mysqlconnect->real_escape_string($code);

		// проверям логин
		if(!preg_match("/^[a-zA-Z0-9]+$/",$this->username))
		{
			$err[] = "Логин может состоять только из букв английского алфавита и цифр";
		}

		if(strlen($this->username) < 3 or strlen($this->username) > 15)
		{
			$err[] = "Логин должен быть не меньше 3-х символов и не больше 15";
		}
		
		// проверяем, не сущестует ли игры с таким кодом
		$query = $this->mysqlconnect->query("SELECT ID FROM sessions WHERE CODE='".$this->code."'");
		if(mysqli_num_rows($query) == 0)
		{
			$err[] = "Игры с таким кодом не существует";
		}
		
		// Если нет ошибок, то добавляем в БД нового пользователя
		if(count($err) == 0)
		{
			$this->mysqlconnect->query("INSERT INTO players SET NAME='".$this->username."', SESSION_ID=(SELECT ID from sessions WHERE CODE='".$this->code."')");
			
			$result['RESULT'] = true;
			$result['MESSAGE'] = 'Вы присоединились к игре.';
		}
		else
		{
            $result['RESULT'] = false;
            $result['MESSAGE'] = $err;
        }
        return $result;
	}
	
	public function logout()
	{
		session_unset();
		session_destroy();
		unset($_COOKIE);
	}
	
	public function host($username, $code)
	{
		$err = [];
		$this->username = $this->mysqlconnect->real_escape_string($username);
		$this->code = $this->mysqlconnect->real_escape_string($code);

		// проверям логин
		if(!preg_match("/^[a-zA-Z0-9]+$/",$this->username))
		{
			$err[] = "Логин может состоять только из букв английского алфавита и цифр";
		}

		if(strlen($this->username) < 3 or strlen($this->username) > 15)
		{
			$err[] = "Логин должен быть не меньше 3-х символов и не больше 15";
		}
		
		// проверям код
		if(!preg_match("/^[a-zA-Z0-9]+$/",$this->code))
		{
			$err[] = "Код может состоять только из букв английского алфавита и цифр";
		}

		if(strlen($this->code) < 3 or strlen($this->code) > 20)
		{
			$err[] = "Код должен быть не меньше 3-х символов и не больше 20";
		}

		// проверяем, не сущестует ли игры с таким кодом
		$query = $this->mysqlconnect->query("SELECT ID FROM sessions WHERE CODE='".$this->code."'");
		if(mysqli_num_rows($query) > 0)
		{
			$err[] = "Не удалось создать игру с таким кодом";
		}
		
		// Если нет ошибок, то добавляем в БД нового пользователя
		if(count($err) == 0)
		{
			$this->mysqlconnect->query("INSERT INTO sessions SET CODE='".$this->code."'");
			$this->mysqlconnect->query("INSERT INTO players SET NAME='".$this->username."', SESSION_ID=(SELECT ID from sessions WHERE CODE='".$this->code."')");
			
			$result['RESULT'] = true;
			$result['MESSAGE'] = 'Игра создана.';
		}
		else
		{
            $result['RESULT'] = false;
            $result['MESSAGE'] = $err;
        }
        return $result;
    }
		
}?>